﻿using System.Windows.Controls;

namespace DB_project.Tools.Navigation
{
    internal interface IContentOwner
    {
        ContentControl ContentControl { get; }
    }
}
